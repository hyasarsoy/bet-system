
//login
var working = false;
$('.login').on('submit', function(e) {
  e.preventDefault();

  if (working) return;

  working = true;

  var $this = $(this),
    $state = $this.find('button > .state');

  $this.addClass('loading');
  $state.html('Bağlanılıyor');

  setTimeout(function() {
    $this.addClass('ok');
    $state.html('Hoş Geldiniz!');

    setTimeout(function() {
      $state.html('Giriş Yap');
      $this.removeClass('ok loading');
      working = false;
    }, 4000);

  }, 3000);
});
